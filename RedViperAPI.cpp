/**********************************************************\

  Auto-generated RedViperAPI.cpp

\**********************************************************/

#include "JSObject.h"
#include "variant_list.h"
#include "DOM/Document.h"
#include "global/config.h"

#include "RedViperAPI.h"

#define BUFFERSIZE 512
#include "b64/encode.h"
#include "b64/decode.h"

#include "tools/certificate-search.h"
#include "csp-defines/itccspdefs.h"


///////////////////////////////////////////////////////////////////////////////
/// @fn FB::variant RedViperAPI::echo(const FB::variant& msg)
///
/// @brief  Echos whatever is passed from Javascript.
///         Go ahead and change it. See what happens!
///////////////////////////////////////////////////////////////////////////////
FB::variant RedViperAPI::echo(const FB::variant& msg)
{
    static int n(0);
    fire_echo("So far, you clicked this many times: ", n++);

    // return "foobar";
    return msg;
}

///////////////////////////////////////////////////////////////////////////////
/// @fn RedViperPtr RedViperAPI::getPlugin()
///
/// @brief  Gets a reference to the plugin that was passed in when the object
///         was created.  If the plugin has already been released then this
///         will throw a FB::script_error that will be translated into a
///         javascript exception in the page.
///////////////////////////////////////////////////////////////////////////////
RedViperPtr RedViperAPI::getPlugin()
{
    RedViperPtr plugin(m_plugin.lock());
    if (!plugin) {
        throw FB::script_error("The plugin is invalid");
    }
    return plugin;
}

// Read/Write property testString
std::string RedViperAPI::get_testString()
{
    return m_testString;
}

void RedViperAPI::set_testString(const std::string& val)
{
    m_testString = val;
}

std::string RedViperAPI::signFile(const std::string& filename) {
	std::vector<BYTE> bytes = RedViperAPI::readFile(filename);
	return RedViperAPI::signData(bytes);
}

std::vector<BYTE> RedViperAPI::readFile(const std::string& filename)
{
    // open the file:
	std::ifstream file(filename.c_str(), std::ios::binary);

    // read the data:
    return std::vector<BYTE>((std::istreambuf_iterator<char>(file)),
                              std::istreambuf_iterator<char>());
}

std::string RedViperAPI::sign(const std::string& val)
{
	std::string decodedStr = decodeB64(val);
	std::vector	<BYTE> message_ = strToBinVec(decodedStr);
	return signData(message_);
}

std::string RedViperAPI::signData(const std::vector<BYTE>& message_) {
	CertificateGuiSearch certificateSearch_;
	certificateSearch_.FindContext();
	if (!certificateSearch_.GetContext())
		return "";
	std::vector<BYTE> detachedSignature_;
	const BYTE* MessageArray[] = { &message_[0] };
	DWORD MessageSizeArray[] = { message_.size() };

	CRYPT_SIGN_MESSAGE_PARA  SigParams = { 0 };

	//Initialize the SignParams data structure
	SigParams.cbSize = sizeof(CRYPT_SIGN_MESSAGE_PARA);
	SigParams.dwMsgEncodingType = MY_ENCODING_TYPE;
	SigParams.pSigningCert = certificateSearch_.GetContext();
	SigParams.HashAlgorithm.pszObjId = szOID_CPCSP_HASH_ALG;
	SigParams.HashAlgorithm.Parameters.cbData = NULL;
	SigParams.cMsgCert = 1;
	SigParams.rgpMsgCert = &certificateSearch_.GetContext();
	SigParams.cAuthAttr = 0;
	SigParams.dwInnerContentType = 0;
	SigParams.cMsgCrl = 0;
	SigParams.cUnauthAttr = 0;
	SigParams.dwFlags = 0;
	SigParams.pvHashAuxInfo = NULL;
	SigParams.rgAuthAttr = NULL;

	DWORD cbDetachedSignatureBlob = 0;

	//Get the size of the output signature blob
	if(!CryptSignMessage(
		&SigParams,                //Signature parameters
		TRUE,                      //Detached
		1,                         //Number of messages
		MessageArray,              //Messages to be signed
		MessageSizeArray,          //Size of messages
		NULL,                      //Buffer for signature
		&cbDetachedSignatureBlob)) //Size of buffer
	{
		DWORD lastError = GetLastError();
		if (!lastError
			|| lastError == SCARD_W_CANCELLED_BY_USER
			|| lastError == ERROR_CANCELLED)
			return "";
		throwEx("Signing error.", lastError);
	}

	detachedSignature_.assign( cbDetachedSignatureBlob, 0 );

	//Sign message and get detached signature in signature blob
	if(!CryptSignMessage(
		&SigParams,               //Signature parameters
		TRUE,                     //Detached
		1,                        //Number of messages
		MessageArray,             //Messages to be signed
		MessageSizeArray,         //Size of messages
		&detachedSignature_[0],  //Buffer for signature
		&cbDetachedSignatureBlob))//Size of buffer
	{
		DWORD lastError = GetLastError();
		if (!lastError
			|| lastError == SCARD_W_CANCELLED_BY_USER
			|| lastError == ERROR_CANCELLED)
			return "";
		throwEx("Signing error.", lastError);
	}
	std::stringstream in, out;
	in.write((char*)&detachedSignature_[0], detachedSignature_.size());
	base64::encoder enc;
	enc.encode(in, out);
	return out.str();
}


std::string RedViperAPI::encodeB64(const std::string& val)
{
	std::stringstream in(val), out;
	base64::encoder enc;
	enc.encode(in, out);
	return out.str();
}

std::string RedViperAPI::decodeB64(const std::string& val)
{
	std::stringstream in(val), out;
	base64::decoder dec;
	dec.decode(in, out);
	return out.str();
}

std::string RedViperAPI::verify(const std::string& dataB64, const std::string& signatureB64) {
	std::string decodedStr = decodeB64(dataB64);
	std::vector<BYTE> message_ = strToBinVec(decodedStr);
	return verifyData(message_, signatureB64);
}

std::string RedViperAPI::verifyFile(const std::string& filename, const std::string& signatureB64) {
	 std::ifstream file(filename.c_str(), std::ios::binary);
	 std::vector<BYTE> bytes = readFile(filename);
	 return verifyData(bytes, signatureB64);
}


std::string RedViperAPI::verifyData(const std::vector<BYTE>& message_, const std::string& signatureB64) {
	 std::string decodedStr = decodeB64(signatureB64);
	 std::vector<BYTE> detachedSignature_ = strToBinVec(decodedStr);
     const BYTE* MessageArray[] = { &message_[0] };
     DWORD MessageSizeArray[] = { message_.size() };

     CRYPT_VERIFY_MESSAGE_PARA VerifyParams = { 0 };

     //Initialize the VerifyParams data structure
     VerifyParams.cbSize = sizeof(CRYPT_VERIFY_MESSAGE_PARA);
     VerifyParams.dwMsgAndCertEncodingType = MY_ENCODING_TYPE;
     VerifyParams.hCryptProv = NULL;
     VerifyParams.pfnGetSignerCertificate = NULL;
     VerifyParams.pvGetArg = NULL;

     //Verify detached message signature
     if(!CryptVerifyDetachedMessageSignature(
          &VerifyParams,           //Verify parameters.
          0,                       //Signer index.
          &detachedSignature_[0], //Pointer to signed BLOB.
          detachedSignature_.size(), //Size of signed BLOB.
          1,                       //Number of messages
          MessageArray,            //Messages to be signed
          MessageSizeArray,        //Size of buffer.
          NULL))                   //Pointer to signer certificate.
     {
		DWORD lastError = GetLastError();
		if (lastError == NTE_BAD_SIGNATURE)
			return "1";
		throwEx("Verifying error.", lastError);
     }
	 return "0";
}

void RedViperAPI::throwEx(const std::string& message, DWORD code) {
	std::ostringstream sream;
	sream << message;
	sream << " Error code = ";
	sream << std::hex << code;
	sream << ".";
	throw FB::script_error(sream.str());
}

std::vector<BYTE> RedViperAPI::strToBinVec(const std::string& str) {
	std::vector<BYTE> vec;
	vec.assign(str.c_str(), str.c_str() + str.length());
	return vec;
}

// Read-only property version
std::string RedViperAPI::get_version()
{
    return FBSTRING_PLUGIN_VERSION;
}

void RedViperAPI::testEvent()
{
    fire_test();
}

/// @file
/// @brief ���� �������� ����������� ������� ������� ������ ������������.
///
/// Copyright (c) InfoTeCS. All Rights Reserved.

#include "certificate-search.h"
#include <JSExceptions.h>



CertificateSearch::CertificateSearch()
: store_( 0 )
, context_( 0 )
{
     OpenStore( 0, _T( "MY" ) );
}

CertificateSearch::~CertificateSearch()
{
     CertCloseStore( store_, 0 );
     CertFreeCertificateContext( context_ );
}

void CertificateSearch::OpenStore( HCRYPTPROV _prov, LPCTSTR _storeName )
{
	CertCloseStore( store_, 0 );

	store_ = CertOpenSystemStore( _prov, _storeName );
    DWORD lastError = GetLastError();
	if( !store_ && lastError
			&& lastError != SCARD_W_CANCELLED_BY_USER
			&& lastError != ERROR_CANCELLED) {
		std::ostringstream sream;
		sream << "CertOpenSystemStore.";
		sream << " Error code = ";
		sream << std::hex << lastError;
		sream << ".";
		throw FB::script_error(sream.str());
	}
}

PCCERT_CONTEXT& CertificateSearch::GetContext()
{
     return context_;
}

HCERTSTORE CertificateSearch::GetStore() const
{
     return store_;
}

void CertificateSearch::SetContext( PCCERT_CONTEXT _context )
{
     context_ = _context;
}

void CertificateGuiSearch::FindContext()
{
	HCERTSTORE store = GetStore();
	if (!store)
		return;
	PCCERT_CONTEXT context = CryptUIDlgSelectCertificateFromStore(
		store,
		NULL,
		NULL,
		NULL,
		CRYPTUI_SELECT_LOCATION_COLUMN,
		0,
		NULL );
    DWORD lastError = GetLastError();
	if( !context && lastError
			&& lastError != SCARD_W_CANCELLED_BY_USER
			&& lastError != ERROR_CANCELLED) {
		std::ostringstream sream;
		sream << "CryptUIDlgSelectCertificateFromStore.";
		sream << " Error code = ";
		sream << std::hex << lastError;
		sream << ".";
		throw FB::script_error(sream.str());
	}
	SetContext( context );
}

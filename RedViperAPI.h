/**********************************************************\

  Auto-generated RedViperAPI.h

\**********************************************************/

#include <string>
#include <sstream>
#include <boost/weak_ptr.hpp>
#include "JSAPIAuto.h"
#include "BrowserHost.h"
#include "RedViper.h"
#include <stdio.h>
#include <Windows.h>
#include <fstream>

#ifndef H_RedViperAPI
#define H_RedViperAPI

class RedViperAPI : public FB::JSAPIAuto
{
public:
    ////////////////////////////////////////////////////////////////////////////
    /// @fn RedViperAPI::RedViperAPI(const RedViperPtr& plugin, const FB::BrowserHostPtr host)
    ///
    /// @brief  Constructor for your JSAPI object.
    ///         You should register your methods, properties, and events
    ///         that should be accessible to Javascript from here.
    ///
    /// @see FB::JSAPIAuto::registerMethod
    /// @see FB::JSAPIAuto::registerProperty
    /// @see FB::JSAPIAuto::registerEvent
    ////////////////////////////////////////////////////////////////////////////
    RedViperAPI(const RedViperPtr& plugin, const FB::BrowserHostPtr& host) :
        m_plugin(plugin), m_host(host)
    {
        registerMethod("echo",      make_method(this, &RedViperAPI::echo));
        registerMethod("testEvent", make_method(this, &RedViperAPI::testEvent));
        
        // Read-write property
        registerProperty("testString",
                         make_property(this,
                                       &RedViperAPI::get_testString,
                                       &RedViperAPI::set_testString));
        
        // Read-only property
        registerProperty("version",
                         make_property(this,
                                       &RedViperAPI::get_version));
		registerMethod("encodeB64",      make_method(this, &RedViperAPI::encodeB64));
		registerMethod("decodeB64",      make_method(this, &RedViperAPI::decodeB64));
		registerMethod("sign",      make_method(this, &RedViperAPI::sign));
		registerMethod("signFile",      make_method(this, &RedViperAPI::signFile));
		registerMethod("verifyFile",      make_method(this, &RedViperAPI::verifyFile));
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// @fn RedViperAPI::~RedViperAPI()
    ///
    /// @brief  Destructor.  Remember that this object will not be released until
    ///         the browser is done with it; this will almost definitely be after
    ///         the plugin is released.
    ///////////////////////////////////////////////////////////////////////////////
    virtual ~RedViperAPI() {};

    RedViperPtr getPlugin();

    // Read/Write property ${PROPERTY.ident}
    std::string get_testString();
    void set_testString(const std::string& val);

    // Read-only property ${PROPERTY.ident}
    std::string get_version();

    // Method echo
    FB::variant echo(const FB::variant& msg);
    
    // Event helpers
    FB_JSAPI_EVENT(test, 0, ());
    FB_JSAPI_EVENT(echo, 2, (const FB::variant&, const int));

    // Method test-event
    void testEvent();

	std::string RedViperAPI::encodeB64(const std::string& val);
	std::string RedViperAPI::decodeB64(const std::string& val);

	std::string RedViperAPI::sign(const std::string& valB64);
	std::string RedViperAPI::signFile(const std::string& filename);

	std::string RedViperAPI::verify(const std::string& dataB64, const std::string& signatureB64);
	std::string RedViperAPI::verifyFile(const std::string& filename, const std::string& signatureB64);

private:
    RedViperWeakPtr m_plugin;
    FB::BrowserHostPtr m_host;

    std::string m_testString;

	std::string RedViperAPI::signData(const std::vector<BYTE>& message_);
	std::string RedViperAPI::verifyData(const std::vector<BYTE>& message_, const std::string& signatureB64);
	void RedViperAPI::throwEx(const std::string& message, DWORD code);
	std::vector<BYTE> RedViperAPI::readFile(const std::string& filename);
	std::vector<BYTE> RedViperAPI::strToBinVec(const std::string& str);
};

#endif // H_RedViperAPI


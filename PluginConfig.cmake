#/**********************************************************\ 
#
# Auto-Generated Plugin Configuration file
# for Red Viper
#
#\**********************************************************/

set(PLUGIN_NAME "RedViper")
set(PLUGIN_PREFIX "RVI")
set(COMPANY_NAME "RedSoft")

# ActiveX constants:
set(FBTYPELIB_NAME RedViperLib)
set(FBTYPELIB_DESC "RedViper 1.0 Type Library")
set(IFBControl_DESC "RedViper Control Interface")
set(FBControl_DESC "RedViper Control Class")
set(IFBComJavascriptObject_DESC "RedViper IComJavascriptObject Interface")
set(FBComJavascriptObject_DESC "RedViper ComJavascriptObject Class")
set(IFBComEventSource_DESC "RedViper IFBComEventSource Interface")
set(AXVERSION_NUM "1")

# NOTE: THESE GUIDS *MUST* BE UNIQUE TO YOUR PLUGIN/ACTIVEX CONTROL!  YES, ALL OF THEM!
set(FBTYPELIB_GUID 23d5dcd1-9d57-52bd-a2aa-6c244560eff9)
set(IFBControl_GUID ebd72a70-7145-51ea-9226-bde64187b315)
set(FBControl_GUID 9f4cd503-9c1a-5f66-b1ad-65a5ebbcd684)
set(IFBComJavascriptObject_GUID d6c913a9-ff77-5756-8d50-9a40262af289)
set(FBComJavascriptObject_GUID a70c3efc-4a84-5f0d-8b50-966fd81da2a6)
set(IFBComEventSource_GUID 37e05d28-ce7b-5867-b23b-7256649f7718)
if ( FB_PLATFORM_ARCH_32 )
    set(FBControl_WixUpgradeCode_GUID cbdca584-39d3-5d3b-9eab-b2498c878c27)
else ( FB_PLATFORM_ARCH_32 )
    set(FBControl_WixUpgradeCode_GUID 97a289a8-656c-555f-b2b4-6ea005e5273d)
endif ( FB_PLATFORM_ARCH_32 )

# these are the pieces that are relevant to using it from Javascript
set(ACTIVEX_PROGID "RedSoft.RedViper")
set(MOZILLA_PLUGINID "red-soft.biz/RedViper")

# strings
set(FBSTRING_CompanyName "Red Soft")
set(FBSTRING_PluginDescription "Red VipNet Plugin")
set(FBSTRING_PLUGIN_VERSION "1.0.0.0")
set(FBSTRING_LegalCopyright "Copyright 2013 Red Soft")
set(FBSTRING_PluginFileName "np${PLUGIN_NAME}")
set(FBSTRING_ProductName "Red Viper")
set(FBSTRING_FileExtents "")
if ( FB_PLATFORM_ARCH_32 )
    set(FBSTRING_PluginName "Red Viper")  # No 32bit postfix to maintain backward compatability.
else ( FB_PLATFORM_ARCH_32 )
    set(FBSTRING_PluginName "Red Viper_${FB_PLATFORM_ARCH_NAME}")
endif ( FB_PLATFORM_ARCH_32 )
set(FBSTRING_MIMEType "application/x-redviper")

# Uncomment this next line if you're not planning on your plugin doing
# any drawing:

#set (FB_GUI_DISABLED 1)

# Mac plugin settings. If your plugin does not draw, set these all to 0
set(FBMAC_USE_QUICKDRAW 0)
set(FBMAC_USE_CARBON 1)
set(FBMAC_USE_COCOA 1)
set(FBMAC_USE_COREGRAPHICS 1)
set(FBMAC_USE_COREANIMATION 0)
set(FBMAC_USE_INVALIDATINGCOREANIMATION 0)

# If you want to register per-machine on Windows, uncomment this line
#set (FB_ATLREG_MACHINEWIDE 1)
